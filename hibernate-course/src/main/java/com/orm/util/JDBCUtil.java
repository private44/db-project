package com.orm.util;

import java.sql.*;

public class JDBCUtil {

	public static void main(String[] args) {
		
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = DriverManager.getConnection("jdbc:postgresql://188.120.239.30:5432/mydb", "postgres", "elisha5cuthbert");
			PreparedStatement prepareStatement = connection.prepareStatement("select * from my_logs");
			ResultSet executeQuery = prepareStatement.executeQuery();
			while(executeQuery.next()){
				System.out.println(executeQuery.getString(3));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
