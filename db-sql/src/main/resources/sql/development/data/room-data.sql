INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 7', 201, 1000, 'Large Lecture Hall');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 6' , 101, 500, 'Small Lecture Hall');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 6', 150, 50, 'Discussion Room A');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 6', 160, 50, 'Discussion Room B');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 6', 170, 50, 'Discussion Room C');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Music Building', 100, 10, 'Music Practice Room');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Music Building', 200, 1000, 'Concert Room');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 7' , 300, 75, 'Discussion Room D');
INSERT INTO rooms (room_id, building, room_number, number_seats, description) VALUES (room_sequence.NEXTVAL, 'Building 7' , 310, 50, 'Discussion Room E') ;


