INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('HIS', 101, 'History 101', 30, 11, 4, 20000);
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('HIS', 301, 'History 30V', 30, 0, 4, 20004); 
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('CCS', 101, 'Computer Science 101', 50, 0, 4, 20001);
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('ECN', 203, 'Economics 203', 15, 0, 3, 20002);
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('CS', 102, 'Computer Science 102', 35, 3, 4, 20003);
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('MUS', 410, 'Music 410', 5, 4, 3, 20005);
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('ECN', 101, 'Economics 101', 50, 0, 4, 20007);
INSERT INTO classes(department, course, description, max_students, current_students, num_credits, room_id) VALUES ('NUT', 307, 'Nutrition 307', 20, 2, 4, 20008);



