-- create script: create (table, patch and deployable scripts)


PROMPT *********************************************************************
PROMPT * Parent script for table objects - baseline                        *
PROMPT * These are the original script for creating the database tables    *
PROMPT *********************************************************************

PROMPT Running the baseline table scripts

PROMPT create table t_user
@.\tables\t_user.sql;



PROMPT Running the baseline sequences scripts
@.\sequences\sequences.sql;
