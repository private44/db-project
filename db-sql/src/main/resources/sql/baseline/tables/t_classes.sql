CREATE TABLE classes (
    department CHAR(3),
    course NUMBER(3),
    description VARCHAR2(2000),
    max_students NUMBER(3),
    current_students NUMBER(3),
    num_credits NUMBER(1),
    room_id NUMBER(5),
   CONSTRAINT classes_department_course PRIMARY KEY (department, course),
   CONSTRAINT classes_room_id FOREIGN KEY (room_id) REFERENCES rooms (room_id)
);


