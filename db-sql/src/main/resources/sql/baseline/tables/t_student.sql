CREATE TABLE students (
  student_id      NUMBER(5) PRIMARY KEY,
  first_name      VARCHAR2(20),
  last_name       VARCHAR2(20),
  major           VARCHAR2(30),
  current_credits NUMBER(3)
);

