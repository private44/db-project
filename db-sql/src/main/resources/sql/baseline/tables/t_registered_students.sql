CREATE TABLE registered_students (
    student_id NUMBER(5) NOT NULL,
    department CHAR(3) NOT NULL,
    course NUMBER(3) NOT NULL,
    grade CHAR(1),
    CONSTRAINT rs_grade CHECK (grade IN ('A' , 'B' , 'C' , 'D' , 'E')) ,
    CONSTRAINT rs_student_id FOREIGN KEY (student_id) REFERENCES students (student_id),
    CONSTRAINT rs_department_course FOREIGN KEY (department, course) REFERENCES classes (department, course)
);



