CREATE TABLE rs_audit (
    change_type CHAR(1) NOT NULL,
    changed_by VARCHAR2(8) NOT NULL,
    rstimestamp DATE NOT NULL,
    old_student_id NUMBER(5),
    old_department CHAR(3),
    old_course NUMBER(3),
    old_grade CHAR(1),
    new_student_id NUMBER(5),
    new_department CHAR(3),
    new_course NUMBER(3),
    new_grade CHAR(1)
);












