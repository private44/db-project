CREATE TABLE rooms (
    room_id NUMBER(5) PRIMARY KEY,
    building VARCHAR2(15),
    room_number NUMBER(4),
    number_seats NUMBER(4),
    description VARCHAR2(50)
);
