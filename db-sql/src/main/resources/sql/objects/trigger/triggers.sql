create or replace TRIGGER only_positive before
  INSERT OR UPDATE OF num_col ON temp_table 
  FOR EACH ROW 
  BEGIN 
      IF :new.num_col < 0 THEN 
          RAISE_APPLICATION_ERROR(-20100, 'Please insert a positive value');
      END IF;
END only_positive;
/

EXIT


