--exec update_discipline_for_student();
--delete STUDENTS where CURRENT_CREDITS is null;
--select * from STUDENTS;
create or replace procedure update_discipline_for_student(p_first_name IN VARCHAR2,  p_last_name IN VARCHAR2)
AS
    v_new_major varchar2(10)  := 'History';
    v_first_name varchar2(10) := p_first_name;
    v_last_name varchar2(10)  := p_last_name;
    begin
        update STUDENTS set MAJOR = v_new_major where FIRST_NAME = v_first_name AND LAST_NAME = v_last_name;

        IF SQL%NOTFOUND THEN
            INSERT INTO students(student_id, first_name, last_name, major) VALUES(student_sequence.NEXTVAL, v_first_name, v_last_name, v_new_major);
            DBMS_OUTPUT.PUT_LINE('Add new student');
        end IF;
    end;
/

--**************************************************** COUNT STUDENTS ***************************************************************************
CREATE OR REPLACE PROCEDURE COUNT_STUDENTS(cnt OUT NUMBER)
AS BEGIN
    SELECT count(*) INTO cnt FROM STUDENTS;
END;
/

--**************************************************** EXCEPTION ********************************************************************************
--exec register_error_in_log_table();
--select * from LOG_TABLE;
create or replace procedure register_error_in_log_table(std_id IN NUMBER)
IS
    v_error_code    number;        --codul erorii
    v_error_message varchar2(200); --message despre eroare
    v_current_user  varchar2(8);   --user current al db
    v_information   VARCHAR2(100); --info despre eroare
    v_first_name    VARCHAR2(100); 
begin   
    select FIRST_NAME into v_first_name from students where STUDENT_ID = std_id;
    
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_error_code := SQLCODE;
        v_error_message := SQLERRM;
        v_current_user := USER;
        v_information := 'Error on ' || TO_CHAR(SYSDATE) || ' by database user ' || v_current_user;
        INSERT INTO LOG_TABLE(LOG_ID, CODE, MESSAGE, INFO) values(log_sequence.nextval, v_error_code, v_error_message, v_information);
        DBMS_OUTPUT.PUT_LINE('Generate Exception');
end;
/

--**************************************************** IF ELSE ********************************************************************************
--exec count_students_reg_temp_table();
--select * from TEMP_TABLE;
create or replace procedure count_students_reg_temp_table
IS
    v_total_students number;
begin
    select count(*) into v_total_students from students;
    
    IF v_total_students = 0 THEN
        INSERT INTO TEMP_TABLE(CHAR_COL) VALUES('Students not registered');
    ELSIF v_total_students < 5 THEN
        INSERT INTO TEMP_TABLE(CHAR_COL) VALUES('Some students registered');
    ELSIF v_total_students < 10 THEN
        INSERT INTO TEMP_TABLE(CHAR_COL) VALUES('Not many students registered');
    ELSE
        INSERT INTO TEMP_TABLE(CHAR_COL) VALUES('Many students registered');
    END IF;
end;
/

--**************************************************** LOOP ********************************************************************************
--exec loop_proc();
--select * from TEMP_TABLE;
create or replace procedure loop_proc
IS
    v_loop_counter BINARY_INTEGER := 1;
begin
    LOOP
        INSERT INTO TEMP_TABLE(num_col) VALUES(v_loop_counter);
        v_loop_counter := v_loop_counter + 1;
        EXIT WHEN v_loop_counter > 50;
    END LOOP;
end;
/

--**************************************************** FOR ********************************************************************************
--exec for_proc();
--select * from TEMP_TABLE;
create or replace procedure for_proc
IS
    v_loop_counter BINARY_INTEGER := 1;
begin
    FOR v_loop_counter IN 1..50 LOOP
        INSERT INTO TEMP_TABLE(num_col) VALUES(v_loop_counter);
    END LOOP;
end;
/

--**************************************************** CURSOR ********************************************************************************
--exec cursor_proc();
create or replace procedure cursor_proc
IS
    v_first_name VARCHAR2(20);
    v_last_name VARCHAR2(20);
    CURSOR c_Students IS SELECT FIRST_NAME, LAST_NAME FROM STUDENTS;
begin
    OPEN c_Students;
    LOOP
        FETCH c_Students INTO v_first_name, v_last_name;
        EXIT WHEN c_Students%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_first_name || ' ' || v_last_name);
    END LOOP;
    CLOSE c_Students;
end;
/



