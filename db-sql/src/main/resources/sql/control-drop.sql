PROMPT *********************************************************************
PROMPT *                      PURGE RECYCLEBIN                             *
PROMPT *********************************************************************

PURGE RECYCLEBIN
/
BEGIN
  DECLARE CURSOR c_get_objects IS
    SELECT
      object_type,
      '"' || object_name || '"' || decode(object_type, 'TABLE', ' cascade constraints', NULL) obj_name
    FROM USER_OBJECTS
    WHERE object_type IN ('TABLE', 'VIEW', 'PACKAGE', 'SEQUENCE', 'SYNONYM', 'MATERIALIZED VIEW', 'TYPE', 'PROCEDURE')
    ORDER BY object_type;
  BEGIN
    FOR object_rec IN c_get_objects LOOP
      EXECUTE IMMEDIATE ('drop ' || object_rec.object_type || ' ' || object_rec.obj_name);
    END LOOP;
  END;
END;
/

EXIT
