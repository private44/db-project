-- create script: create (baseline, deployable and development scripts)

WHENEVER SQLERROR EXIT FAILURE
WHENEVER OSERROR EXIT FAILURE

-- SPOOL ${control.log.dir}/teask_data_schema_create.log

PROMPT *********************************************************************
PROMPT *               Create sequences script                             *
PROMPT *********************************************************************
@.\baseline\sequences\sequences.sql;

PROMPT *********************************************************************
PROMPT *               Create table script                                 *
PROMPT *********************************************************************
@.\baseline\tables\t_student.sql;
@.\baseline\tables\t_major_stats.sql;
@.\baseline\tables\t_rooms.sql;
@.\baseline\tables\t_classes.sql;
@.\baseline\tables\t_registered_students.sql;
@.\baseline\tables\t_rs_audit.sql;
@.\baseline\tables\t_log_table.sql;
@.\baseline\tables\t_temp_table.sql;



PROMPT *********************************************************************
PROMPT *               Create views scripts                                *
PROMPT *********************************************************************
@.\deployable\views\v_student.sql
@.\deployable\views\v_major-stats.sql
@.\deployable\views\v_rooms.sql
@.\deployable\views\v_classes.sql
@.\deployable\views\v_registered_students.sql
@.\deployable\views\v_rs_audit.sql
@.\deployable\views\v_log_table.sql
@.\deployable\views\v_temp_table.sql

PROMPT *********************************************************************
PROMPT *               Create grants script                                *
PROMPT *********************************************************************
@.\deployable\grants\grants.sql

PROMPT *********************************************************************
PROMPT *               Insert data...                                      *
PROMPT *********************************************************************
@.\development\data\student-data.sql
@.\development\data\major-stats-data.sql
@.\development\data\room-data.sql
@.\development\data\class-data.sql
@.\development\data\registered-students-data.sql

PROMPT *********************************************************************
PROMPT *               Create procedure scripts                            *
PROMPT *********************************************************************
@.\objects\procedure\procedures.sql

PROMPT *********************************************************************
PROMPT *               Create trigger scripts                            *
PROMPT *********************************************************************
@.\objects\trigger\triggers.sql

SPOOL OFF
EXIT