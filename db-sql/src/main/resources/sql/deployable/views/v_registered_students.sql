CREATE OR REPLACE VIEW V_REGISTERED_STUDENTS(
     student_id,
     department,
     course,
     grade
) AS SELECT
       student_id,
       department,
       course,
       grade
     FROM
       registered_students;
