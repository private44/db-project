CREATE OR REPLACE VIEW V_MAJOR_STATS(
     major,
     total_credits,
     total_students
) AS SELECT
       major,
       total_credits,
       total_students
     FROM
       major_stats;
