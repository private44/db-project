CREATE OR REPLACE VIEW V_CLASSES(
     department,
     course,
     description,
     max_students,
     current_students,
     num_credits,
     room_id
) AS SELECT
       department,
       course,
       description,
       max_students,
       current_students,
       num_credits,
       room_id
     FROM
       classes;
