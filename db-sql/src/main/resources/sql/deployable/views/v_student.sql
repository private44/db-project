CREATE OR REPLACE VIEW V_STUDENT(
    student_id,
    first_name,
    last_name,
    major,
    current_credits
) AS SELECT
       student_id,
       first_name,
       last_name,
       major,
       current_credits
     FROM
       students;
