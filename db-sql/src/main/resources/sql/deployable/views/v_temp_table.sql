CREATE OR REPLACE VIEW V_TEMP_TABLE(
     num_col,
     char_col
) AS SELECT
       num_col,
       char_col
     FROM
       temp_table;
