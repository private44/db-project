CREATE OR REPLACE VIEW V_LOG_TABLE(
     code,
     message,
     info
) AS SELECT
       code,
       message,
       info
     FROM
       log_table;
