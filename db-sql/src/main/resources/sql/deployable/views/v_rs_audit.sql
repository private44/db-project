CREATE OR REPLACE VIEW V_RS_AUDIT(
     change_type,
     changed_by,
     rstimestamp,
     old_student_id,
     old_department,
     old_course,
     old_grade,
     new_student_id,
     new_department,
     new_course,
     new_grade
) AS SELECT
       change_type,
       changed_by,
       rstimestamp,
       old_student_id,
       old_department,
       old_course,
       old_grade,
       new_student_id,
       new_department,
       new_course,
       new_grade
     FROM
       rs_audit;
