CREATE OR REPLACE VIEW V_ROOM(
    room_id,
    building,
    room_number,
    number_seats,
    description
) AS SELECT
       room_id,
       building,
       room_number,
       number_seats,
       description
     FROM
       rooms;
